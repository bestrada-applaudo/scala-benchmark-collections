package com.scala.benchmark.util

object ExecutionTime {
  def time[R](block: => (String, R)): Unit = {
    //memInfo()
    println("Start ====")
    var t0 = System.currentTimeMillis()
    var result = block._2
    println("End ======")
    var t1 = System.currentTimeMillis()
    //println("Memory usage: " + memInfo())
    println(block._1 + " - Time: " + (t1 - t0) + " Millis")
    println()
  }

  /*def memInfo(): String ={
    System.gc()
    val mem = Runtime.getRuntime.totalMemory() - Runtime.getRuntime.freeMemory()
    (  mem / 1024.0 / 1024.0 + " MB")
  }*/
}
