package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

object Reduce {
  val range1 = (1 to 10000)
  val list1 = range1.toList
  val vector1 = range1.toVector
  val seq1 = range1.toSeq
  val array1 = range1.toArray

  lazy val list = {
    println("List")
    val result = list1.reduceRight((x, y) => x + y)
    //println(result)
  }

  lazy val vector = {
    println("vector")
    val result = vector1.reduceRight((x, y) => x + y)
    //println(result)
  }

  lazy val seq = {
    println("Seq")
    val result = seq1.reduceRight((x, y) => x + y)
    //println(result)
  }

  lazy val array = {
    println("Array")
    val result = array1.reduceRight((x, y) => x + y)
    //println(result)
  }

  def main(args: Array[String]): Unit = {

    ExecutionTime.time {
      ("Reduce operation", array)
    }
  }
}
