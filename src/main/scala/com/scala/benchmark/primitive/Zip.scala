package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

object Zip {
  val range1 = (1 to 1000000)
  val range2 = (5000000 to 10000000)
  val list1 = range1.toList
  val list2 = range2.toList
  val vector1 = range1.toVector
  val vector2 = range2.toVector
  val seq1 = range1.toSeq
  val seq2 = range2.toSeq
  val array1 = range1.toArray
  val array2 = range2.toArray

  def main(args: Array[String]): Unit = {
    ExecutionTime.time {
      ("Zip operation", vector)
    }
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1 zip list2
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1 zip vector2
    //println(list3)
  }

  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1 zip seq2
    //println(seq3)
  }

  lazy val array: Unit = {
    println("Array")
    val array3 = array1 zip array2
    //println(seq3)
  }
}
