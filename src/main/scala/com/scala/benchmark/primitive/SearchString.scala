package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

import scala.io.Source

object SearchString {
  var file1 = Source.fromFile("/Text1.txt").mkString
  var file2 = Source.fromFile("/Text2.txt").mkString
  file1 *= 1
  file2 *= 1

  val range1 = file1.split(" ")
  val list1 = range1.toList
  val vector1 = range1.toVector
  val seq1 = range1.toSeq
  val array1 = range1.toArray


  val searchValue = "finibus"

  def main(args: Array[String]): Unit = {
    //println(range1.size)
    ExecutionTime.time {
      ("Search operation", array)
    }
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1.indexOf(searchValue)
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1.indexOf(searchValue)
    //println(list3)
  }

  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1.indexOf(searchValue)
    //println(seq3)
  }

  lazy val array: Unit = {
    println("Array")
    val array3 = array1.indexOf(searchValue)
    //println(seq3)
  }
}
