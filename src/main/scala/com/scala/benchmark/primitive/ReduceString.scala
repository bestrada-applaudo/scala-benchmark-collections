package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

import scala.io.Source

object ReduceString {
  var file1 = Source.fromFile("/Text1.txt").mkString
  var file2 = Source.fromFile("/Text2.txt").mkString
  file1 *= 10
  file2 *= 10

  val range1 = file1.split(" ")
  val list1 = range1.toList
  val vector1 = range1.toVector
  val seq1 = range1.toSeq
  val array1 = range1.toArray

  lazy val list = {
    println("List")
    val result = list1.foldRight("")(_ + _)
    //println(result)
  }

  lazy val vector = {
    println("vector")
    val result = vector1.foldRight("")(_ + _)
    //println(result)
  }

  lazy val seq = {
    println("Seq")
    val result = seq1.foldRight("")(_ + _)
    //println(result)
  }

  lazy val array = {
    println("Array")
    val result = array1.foldRight("")(_ + _)
    //println(result)
  }

  def main(args: Array[String]): Unit = {

    ExecutionTime.time {
      ("Reduce operation", vector)
    }
  }
}
