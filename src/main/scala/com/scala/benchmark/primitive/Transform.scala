package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

object Transform {
  val range1 = (1 to 5000000)

  val list1 = range1.toList
  val vector1 = range1.toVector
  val seq1 = range1.toSeq
  val array1 = range1.toArray

  def main(args: Array[String]): Unit = {
    //println(range1.size)
    ExecutionTime.time {
      ("Search operation", array)
    }
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1.toArray
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1.toArray
    //println(list3)
  }

  //
  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1.toArray
    //println(seq3)
  }

  lazy val array: Unit = {
    println("Array")
    val array3 = array1.toSeq
    //println(seq3)
  }
}
