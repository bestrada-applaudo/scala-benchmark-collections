package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

object Search {
  val range1 = (1 to 10000)

  val list1 = range1.toList
  val vector1 = range1.toVector
  val seq1 = range1.toSeq
  val array1 = range1.toArray

  val searchValue = 114793

  def main(args: Array[String]): Unit = {
    //println(range1.size)
    ExecutionTime.time {
      ("Search operation", list)
    }
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1.contains(searchValue)
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1.contains(searchValue)
    //println(list3)
  }

  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1.contains(searchValue)
    //println(seq3)
  }

  lazy val array: Unit = {
    println("Array")
    val array3 = array1.contains(searchValue)
    //println(seq3)
  }
}
