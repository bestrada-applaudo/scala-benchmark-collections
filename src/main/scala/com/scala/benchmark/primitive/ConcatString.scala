package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

import scala.io.Source

object ConcatString {
  var file1 = Source.fromFile("/Text1.txt").mkString
  var file2 = Source.fromFile("/Text2.txt").mkString
  file1 *= 1
  file2 *= 1

  val range1 = file1.split(" ")
  val range2 = file2.split(" ").reverse
  val list1 = range1.toList
  val list2 = range2.toList
  val vector1 = range1.toVector
  val vector2 = range2.toVector
  val seq1 = range1.toSeq
  val seq2 = range2.toSeq
  val array1 = range1.toArray
  val array2 = range2.toArray

  def main(args: Array[String]): Unit = {
    //println(range1.size)
    ExecutionTime.time {
      ("Prepend operation", list)
    }
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1 ++ list2
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1 ++ vector2
    //println(list3)
  }

  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1 ++ seq2
    //println(seq3)
  }

  lazy val array: Unit = {
    println("Array")
    val array3 = array1 ++ array2
    //println(seq3)
  }
}
