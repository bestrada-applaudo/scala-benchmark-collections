package com.scala.benchmark.primitive

import com.scala.benchmark.util.ExecutionTime

object Combine {
  val range1 = (1 to 10000)
  val range2 = (10000 to 20000)
  val list1 = range1.toList
  val list2 = range2.toList
  val vector1 = range1.toVector
  val vector2 = range2.toVector
  val seq1 = range1.toSeq
  val seq2 = range2.toSeq
  val array1 = range1.toArray
  val array2 = range2.toArray


  def main(args: Array[String]): Unit = {
    //println(range1.size)
    ExecutionTime.time {
      ("Combine operation", array)
    }
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1 zip list2 map { case (a, b) => a + b }
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1 zip vector2 map { case (a, b) => a + b }
    //println(list3)
  }

  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1 zip seq2 map { case (a, b) => a + b }
    //println(seq3)
  }

  lazy val array: Unit = {
    println("Array")
    val array3 = array1 zip array2 map { case (a, b) => a + b }
    //println(seq3)
  }
}
