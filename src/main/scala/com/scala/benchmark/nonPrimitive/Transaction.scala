package com.scala.benchmark.nonPrimitive

import com.scala.benchmark.util.ExecutionTime
import scala.collection.mutable.ListBuffer


case class Transaction(id: Int, client: String, amount: Double) {
  //methods
}

abstract class RList[+T] { //Our list
  def head: T
  def tail: RList[T]
}

case object RNil extends RList[Nothing] {
  override def head: Nothing = throw new NoSuchElementException
  override def tail: RList[Nothing] = throw new NoSuchElementException
}

case class TList[Transaction](override val head: Transaction, override val tail: RList[Transaction]) extends RList[Transaction] {
  def prepend[S >: Transaction](elem: S): TList[S] = {
    new TList(elem, this)
  }

  /*  def zip[A, B](xs: TList[A], ys: TList[B]): TList[(A, B)] =
      (xs, ys) match {
        case (RNil, _) => RNil
        case (_, RNil) => RNil
        case (x :: xs, y :: ys) => (x, y) :: zip(xs, ys)
      }*/
  /*override def toString: String = {
    "WIP"
  }*/

}

object CaseTest extends App {
  val t1 = Transaction(1, "User c1", 12)
  val t2 = Transaction(2, "User c2", 13)
  val t3 = Transaction(3, "User c3", 14)
  val t4 = Transaction(4, "User c4", 15)
  val t5 = Transaction(5, "User c5", 16)
  val t6 = Transaction(6, "User c6", 17)

  val listT = TList(t1, TList(t2, TList(t3, TList(t4, TList(t5, RNil)))))
  println(listT)

  var listBuffer1 = new ListBuffer[Transaction]()
  var listBuffer2 = new ListBuffer[Transaction]()
  val size = 5000000
  for (i <- 1 to size) {
    listBuffer1 = listBuffer1 += Transaction(i, "User " + i, i + 1)
    listBuffer2 = listBuffer2 += Transaction(i + size, "User " + size, i + size + 1)
  }
  val list1 = listBuffer1.toList
  val list2 = listBuffer2.toList
  val vector1 = listBuffer1.toVector
  val vector2 = listBuffer2.toVector
  val seq1 = listBuffer1.toSeq
  val seq2 = listBuffer2.toSeq
  val array1 = listBuffer1.toArray
  val array2 = listBuffer2.toArray

  val searchValue = Transaction(7003, "User 7003", 7004)

  ExecutionTime.time {
    ("Combinations operation", list)
  }

  lazy val list: Unit = {
    println("List")
    val list3 = list1 zip list2 map { case (a, b) => {
      if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
      else (a, b)
    }
    }
    //println(list3)
  }

  lazy val vector: Unit = {
    println("Vector")
    val vector3 = vector1.contains(searchValue)
    //println(list3)
  }
  lazy val seq: Unit = {
    println("Seq")
    val seq3 = seq1.contains(searchValue)
    //println(seq3)
  }
  lazy val array: Unit = {
    println("Array")
    val array3 = array1.contains(searchValue)
    //println(seq3)
  }

  /**
   * Combine (zip map)
   */
  /*list1 zip list2 map{case(a,b)=>{
    if(a.client==b.client)Transaction(0,a.client,a.amount+b.amount)
    else (a,b)
  }}*/

  /**
   * Reduce
   */
  /*list1.reduceLeft((a,b)=>{
    if(a.client==b.client)Transaction(0, a.client, (a.amount+b.amount))
    else a
  })*/

  /**
   * Fold
   */
  /*list1.foldLeft(Transaction(0,"",0)) {
    case (a, b) => {
      if (a.client == b.client) Transaction(0, a.client, a.amount + b.amount)
      else a
    }
  }*/
}